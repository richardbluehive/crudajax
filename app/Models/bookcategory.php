<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class bookcategory extends Eloquent
{
   	protected $connection = 'mongodb';
   	protected $table = 'tbl_book_genre';
   	public $timestamps 	= true;
    protected $guarded = [];

    public function posts()
    {
      return $this->belongsTo('App\Models\Post','_id','title');
    }

    public function category()
    {
      return $this->belongsTo('App\Models\category','genre_id','_id');
    }
}
