<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Post extends Eloquent
{
      //
    protected $connection = 'mongodb';	
    protected $guarded = [];
    protected $fillable =['title', 'content', 'created_by','image','loop'];
 
    public function categories()
    {
      return $this->belongsTo('App\Models\Post');
    }

    public function book_genre()
    {
       return $this->hasmany('App\Models\bookcategory', 'post_id', '_id')->with('category');
    }  




}
   

