<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class category extends Eloquent
{
    protected $connection = 'mongodb';
   	
    protected $guarded = [];

    protected $fillable =[
   	'genre', 'content', 'created_by',
   ];

  
    public function post(){

      return $this->hasMany('App\Models\Post');

    }
}
