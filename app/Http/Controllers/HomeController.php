<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\category;
use App\Models\bookcategory;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['posts']          = Post::with('book_genre')->get(); 
        $data['categories']     = category::all();
        $data['bookcategories'] = bookcategory::all();
        return view('home',$data);
    }
}