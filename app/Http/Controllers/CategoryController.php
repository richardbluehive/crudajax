<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\category;

class CategoryController extends Controller
{
    //
     public function category($_id = false){
    
    	if($_id){

            $data['category'] = category::findOrFail($_id); 

            return view('post.category',$data);
    	}
       

    	return view('post.category');
    }


     public function save(Request $request){

    	//dd($request->title);
    	$data = new category($request -> all());
    
    	$data->save();

    	if($data){

    		return redirect()->route('home.dashboard');
    	}else{
    		return back();
    	}
    }


public function update(Request $request, $cat_id){

        $data =  category::findOrFail($cat_id);
        $data->genre = $request->genre;
        $data->content = $request->content;
        
        $data->save();

        if($data){

            return redirect()->route('home.dashboard');
        }else{
            return back();
        }
    }

     public function delete($cat_id){
        $data = category::destroy($cat_id);
        if($data){
            return redirect()->route('home.dashboard');
        }else{

            dd('error cannot delete this post');

        }

    }

    

}
