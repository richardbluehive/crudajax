<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\bookcategory;
use App\Models\bookcat;
use App\Models\Post;
use App\Models\category;

class BookCategoryController extends Controller
{
     public function bookcategory(){
   
    	
      $results['bookcategories'] = category::with('Post')->get();

      $post = Post::find($post);
      $category = category::find($category);
      return view('home')->with('post', $book->post);
    }

    
    public function bookcat($_id){
        
        if($_id){

         $data ['post'] =  Post::findOrFail($_id);
      
        $data['categories'] = category::get();
            return view('post.bookCategory',$data);
        }
        
 
        else
        {
            return redirect()->route('home');
        }
        
    }
    
    
    public function add_genre(Request $request)
    {
        $data ['post']   = Post::with('book_genre')->findOrFail($request->_id);
        $data['categories'] = category::all()->toArray();
        $data['book_genre'] = $data['post']->book_genre->keyBy('genre_id');


        return view('post.modal', $data);
    }

   

    public function save(Request $request)
    {
        try
        {
          bookcategory::where('post_id', $request->post_id)->delete();
          $_genre = $request->bookcat;

          if($_genre)
          {
            foreach($_genre as $genre)
            {
                $book = new bookcategory;
                $book->post_id = $request->post_id;
                $book->genre_id = $genre;
                $book->save();
            }
          }
          
        }
        catch(\Exception $e)
        {
            dd($e->getMessage());
        }
    } 

     public function loop(){

      
        return view('post.loop');
        
    }

    public function save_loop()
    {
       
         for ($x = 0; $x <= 5000; $x++) {
        $data = new category;
        $data->loop = $x;
        $data->genre = "loop number:".$x;

        $data->save();
      }
        $category = category::count();

        return redirect()->route('home.dashboard');

    }

 
    
}
