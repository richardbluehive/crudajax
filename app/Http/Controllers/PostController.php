<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Post;
use Validator;
class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function form($_id = false){
    	if($_id){

 		$data['post'] = Post::findOrFail($_id); 

 		 return view('post.form',$data);
    	}

    	return view('post.form');
    }

    public function save(Request $request){

    	
      	$data =  new Post();
        $data->title    = $request->input('title');
        $data->content = $request->input('content');

    	

    	if($request->hasfile('image'))
        {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = time() .  '.' .  $extension;
            $file->move('uploads/books', $filename);
            $data->image = 'uploads/books' .  $filename;
    	}
        else
        {
    		
            $data->image = '';

            dd($data);
    	}

        $data->save();
        return redirect()->route('home.dashboard');
    }


    public function update(Request $request, $_id){

    	$data =  Post::findOrFail($_id);
    	$data->title = $request->title;
    	$data->content = $request->content;
    	
    	$data->save();

    	if($data){

    		return redirect()->route('home.dashboard'); 
    	}else{
    		return back();
    	}
    }


    public function delete($_id){
    	$data = Post::destroy($_id);
    	if($data){
    		return redirect()->route('home.dashboard');
    	}else{

    		dd('error cannot delete this post');
    	}

    

    }
    //view to upload image
    public function add_image(Request $request)
    {

       
        $data ['post'] =  Post::findOrFail($request->_id);

        return view('post.imageModal',$data);
       

    }

    //save the uploaded image
   public function action(Request $request)
    {
     $validation = Validator::make($request->all(), [
      'select_file' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
     ]);
      //$data = new Post;
     if($validation->passes())
     {
     
      

      $image = $request->file('select_file');
      $new_name = rand() . '.' . $image->getClientOriginalExtension();
      $image->move(public_path('images'), $new_name);
      return response()->json([

       'message'   => 'Image Upload Successfully',
       'uploaded_image' => '<img src="/images/'.$new_name.'" class="img-thumbnail" width="300" />',
       'class_name'  => 'alert-success'

      ]);

     
     }

     else
     {
      return response()->json([

       'message'   => $validation->errors()->all(),
       'uploaded_image' => '',
       'class_name'  => 'alert-danger'

      ]);
     }
      dd($image);
    }


} 
