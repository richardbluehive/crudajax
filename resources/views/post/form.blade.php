@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Book Form</div>

                <div class="panel-body"> 
                   @if(isset($post))
                    <form action="{{ route('Post.update',$post->_id) }}" method="POST" >
                        {!! csrf_field() !!}
                          <div class="form-group">
                            <label for="usr">Title:</label>
                            <input type="text" class="form-control" name="title" value="{{ $post->title }}">

                      
                          </div>
                          <div class="form-group">
                            <label for="comment">Content:</label>
                            <textarea class="form-control" rows="5" name="content" >{{ $post->content }}</textarea> 
                          </div>


                        <p align="center"><button class="btn btn-primary">SAVE</button></p>
                        </form>
                    @else
                        <form action="{{ route('Post.save') }}" method="post" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                          <div class="form-group">
                            <label for="usr">Title:</label>
                            <input type="text" class="form-control" name="title">

                      
                          </div>
                          <div class="form-group">
                            <label for="comment">Content:</label>
                            <textarea class="form-control" rows="5" name="content" ></textarea> 
                          </div>

                     <div class="input-group">
           
                     <div class="custom-file">
                     <input type="file" name="image" id="select_file">
                     
                     </div>
                      <br><br>

                        <p align="center"><button class="btn btn-primary">SAVE</button></p>
                        </form>

                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
 