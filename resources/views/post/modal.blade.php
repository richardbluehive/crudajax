<form class="form-submit" action="{{route('bookcat.save')}}" method="POST">
	{!! csrf_field() !!}
	<div class="modal-header">
		<input type="hidden" value="{{$post->_id}}" name="post_id"> 
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title">Choose Genre</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<label for="usr"><h4>Title:</h4></label>
	        	<input type="text" class="form-control" name="title" value="{{ $post->title }}" readonly>
			</div>
	        
	    </div>
	    <div class="row">
	    	<div class="col-md-12">
	    		<h4>Genres:</h4>
	    		<ul class="list-group">
      		    @foreach($categories as $bookcats)
         	      	
         	      	<li class="list-group-item">
         	      		<div class="checkbox">
         	      			<label class="form-check-label" for="gridCheck1">
         	      				<input class="form-check-input" type="checkbox" value="{{$bookcats['_id'] }}" class="bookcat" name="bookcat[]" {{ isset($book_genre[$bookcats["_id"]]) == true ? "checked='checked'" : "a" }} >
			          	       {{ $bookcats['genre']}} 
	      	   	  		    </label>
         	      		</div>
			            
			            
      	   	  		</li>
        		  @endforeach
              	</ul>
	    	</div>
	    </div>
	</div>
	<div class="modal-footer">
		<button type="submit" class="btn btn-primary">Save</button>
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	</div>
</form>