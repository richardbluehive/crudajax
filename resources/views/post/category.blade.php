@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Category Form</div>

                <div class="panel-body"> 
                   @if(isset($category))
                    <form action="{{ route('category.update',$category->_id) }}" method="POST">
                        {!! csrf_field() !!}
                          <div class="form-group">
                            <label for="usr">Genre:</label>
                            <input type="text" class="form-control" name="genre" value="{{ $category->genre }}">

                      
                          </div>
                          <div class="form-group">
                            <label for="comment">Content:</label>
                            <textarea class="form-control" rows="5" name="content" >{{ $category->content }}</textarea> 
                          </div>
                        <p align="center"><button class="btn btn-primary">SAVE</button></p>
                        </form>
                    @else
                        <form action="{{ route('category.save') }}" method="post">
                        {!! csrf_field() !!}
                          <div class="form-group">
                            <label for="usr">Genre:</label>
                            <input type="text" class="form-control" name="genre">

                      
                          </div>
                          <div class="form-group">
                            <label for="comment">Content:</label>
                            <textarea class="form-control" rows="5" name="content" ></textarea> 
                          </div>
                        <p align="center"><button class="btn btn-primary">SAVE!</button></p>
                        </form>

                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
 