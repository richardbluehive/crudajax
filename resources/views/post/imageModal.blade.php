

<form class="form-submitImage" action="{{route('action_image')}}" method="POST" enctype="multipart/form-data">
	{!! csrf_field() !!}
	<div class="modal-header">
		<input type="hidden" value="{{$post->_id}}" name="post_id"> 
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title">Add Image</h4>
	</div>
	<div class="modal-body">

				
				<div class="col-md-12">
					<label for="usr"><h4>Title:</h4></label>
		        	<input type="text" class="form-control" name="title" value="{{ $post->title }}" readonly>
				</div>

		  		<div class="input-group">
		  			<br><br>
                     <div class="custom-file">
                     <input type="file" name="select_file" id="select_file">
                     
                     </div>
                </div>

				
	   
	</div>
	<div class="modal-footer">
		<input type="submit" name="upload" id="upload" value="Upload" class="btn btn-primary">

		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	</div>
</form>
