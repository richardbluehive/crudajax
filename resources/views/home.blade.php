@extends('layouts.app')

@section('content')


<div class="alert"  id="message" style="display: none"></div>
<span id="upload_image"></span>
<div class="container">
    <div class="row">
        <div class="col-md-12 col-lf-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">BOOKS</div>

                <div class="panel-body">

                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    
                    <TABLE class="table table-striped ">
                        <thead>
                            <tr>
                                <th>No</th>
                               
                                <th>Title</th>
                                <th>Created At</th>
                                <th>Pic</th>
                                <th>Action</th>
                            </tr>


                        </thead>
                        <tbody>
                         <tr>
                            <?php $no = 1;?>
                            @foreach($posts as $key => $post)
                            <td>{{ $no}}</td>
                            
                            <td>{{ $post->title}}</td>

                            <td>{{ $post->created_at}}</td>
                            <td><img src="{{$post->image}}" style="width: 100px"></td>
                            
                            <td>
                               <button class="btn btn-primary btn-sm btn-modal" data-toggle="modal" data-id="{{$post->_id}}" data-link="{{route('add_genre')}}" data-token="{{csrf_token()}}" data-target="#myModal" data-content=".custom-modal">Add Genre</button>


                                <button class="btn btn-warning btn-sm btn-ModalImage" data-toggle="modal" data-id="{{$post->_id}}" data-link="{{route('add_image')}}" data-token="{{csrf_token()}}" data-target="#ModalImage" data-content=".custom-modalImage">Add Image</button>


                              
                                <a class="btn btn-primary btn-sm" href="{{ route('post.form',$post->_id) }}">Update</a>

                                <a class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete this post?')" href="{{ route('post.delete',$post->_id) }}">Delete</a>
                            </td>

                         </tr>
                            <?php $no++;?>
                            @endforeach
                        </tbody>

                    </TABLE>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Category</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    
                    <TABLE class="table table-striped ">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Genre</th>
                                <th>Created At</th>
                               
                                <th>Action</th>
                            </tr>


                        </thead>
                        <tbody>
                         <tr>
                            @if(isset($categories))
                            <?php $no1 = 1;?>
                            @foreach($categories as $key => $category)
                            <td>{{ $no1}} {{$category->cat_id}}</td>
                            <td>{{ $category->genre}}</td>
                            <td>{{ $category->created_at}}</td>

                            <td>
                                <a class="btn btn-primary btn-sm" href="{{route('post_category',$category->_id)}}">Update</a>


                                <a class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete this post?')" href="{{ route('category.delete',$category->_id) }}">Delete</a>
                               
                            </td>
                         </tr>
                            <?php $no1++;?>
                            @endforeach
                            @endif
                        </tbody>

                    </TABLE>
                </div>
            </div>
        </div>
    </div>
</div>     
        
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content custom-modal">
    </div>

  </div>
</div>

<!-- Modal -->
<div id="ModalImage" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content custom-modalImage">
    </div>

  </div>
</div>







    <!---- books with Category--->
 <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Books with Category</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    
                    <TABLE class="table table-striped ">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Genre</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($posts as $key => $value)
                                <tr>
                                  <td>{{ $value->title }}</td>
                                  <td>
                                    @if(isset($value->book_genre))
                                        @foreach($value->book_genre as $b_key => $b_value)
                                          @if(isset($b_value->category->genre))
                                          {{ $b_value->category->genre }}
                                          @endif
                                        @endforeach
                                    @endif
                                  </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </TABLE>
                </div>
            </div>
        </div>
    </div>
</div>
  
@endsection



@section('js')
<script >


// add categories 
   $(document).ready(function(){
      $(".btn-modal").on('click', function(){
          var link    = $(this).data('link');
          var _token  = $(this).data('token');
          var id      = $(this).data('id');
          var content  = $(this).data('content');
          // console.log(target);
          $.ajax({
            url   :   link,
            type  :   'POST',
            data  :   {
              '_token' : _token,
              '_id' : id
            },
            success :   function(result)
            {
              $(content).html(result);
              submit_form();
            },
            error   :   function(err)
            {
              alert('Error, something went wrong.');
            }
          });
      });

      function submit_form()
      {
            
          $(".form-submit").on('submit', function(e){
              e.preventDefault();
              var link      = $(this).attr('action');
              var method    = $(this).attr('method');
              var formdata  = $(this).serialize();
              $.ajax({
                url   :   link,
                type  :   method,
                data  :   formdata,
                success :   function(result)
                {
                  location.reload();
                },
                error   :   function(err)
                {
                  alert('Error, something went wrong');
                }
              });
          });
      }

   });
// upload image 
 $(document).ready(function(){
      $(".btn-ModalImage").on('click', function(){
          var link    = $(this).data('link');
          var _token  = $(this).data('token');
          var id      = $(this).data('id');
          var content  = $(this).data('content');
          // console.log(target);
          $.ajax({
            url   :   link,
            type  :   'POST',
            data  :   {
              '_token' : _token,
              '_id' : id
            },
            success :   function(result)
            {
              $(content).html(result);

              submit_formImage();
            },
            error   :   function(err)
            {
              alert('Error, something went wrong, Please try again.');
            }
          });
      });

      function submit_formImage()
      {
            
          $(".form-submitImage").on('submit', function(e){
              e.preventDefault();
            
              $.ajax({
                data: new FormData(this),
                Type:'POST',
                contentType: false,
                cache: false,
                processData: false,
                success :   function(result)
                {

                $('#message').css('display','block')
                $('#message').html(data.message);
                $('#message').addClass(data.class_name);
                $('#uploaded_image').html(data.uploaded_image);
                  location.reload();
                },
                error   :   function(err)
                {
                  alert('Error, something went wrong Please try again.');
                }
              });
          });
      }

   });
 
</script>
@endsection



