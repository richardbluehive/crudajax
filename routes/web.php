<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
//Post
Route::get('/home', 'HomeController@index')->name('home.register');
Route::get('/dashboard', 'HomeController@index')->name('home.dashboard');
Route::get('/book/{_id?}', 'PostController@form')->name('post.form');
Route::post('/book/save/', 'PostController@save')->name('Post.save');
Route::post('/book/update/{_id}', 'PostController@update')->name('Post.update');
Route::get('/book/delete/{_id}', 'PostController@delete')->name('post.delete');

//category
Route::get('/categories/{_id}', 'CategoryController@category')->name('post_category');
Route::post('/categories/save', 'CategoryController@save')->name('category.save');
Route::get('/categories/delete/{cat_id}','CategoryController@delete')->name('category.delete');
Route::post('/categories/update/{cat_id}', 'CategoryController@update')->name('category.update');

//bookcategory


Route::get('/bookcategory', 'BookCategoryController@bookcategory')->name('bookcat.home');
Route::post('/bookcat/save','BookCategoryController@save')->name('bookcat.save');



Route::any('/book/add-genre','BookCategoryController@add_genre')->name('add_genre');

Route::any('/book/add-image','PostController@add_image')->name('add_image');
Route::post('/book/action-image','PostController@action')->name('action_image');



Route::get('ajax_upload/action', 'AjaxUploadController@action')->name('ajax_upload_action');

Route::get('/loop', 'BookCategoryController@loop')->name('post.loop');
Route::any('/loop/save', 'BookCategoryController@save_loop')->name('loop_save');
